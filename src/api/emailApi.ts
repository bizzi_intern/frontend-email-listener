import axios from 'axios';
import { BASE_URI } from 'src/lib/constants';

axios.defaults.withCredentials = true;

export const getEmailsWithAttachments = async (accessToken: string) => {
    const query = `#graphql
        query getEmailsWithAttachments {
            emailsWithAttachments {
                id
                from
                sender_name
                to
                date
                subject
                content_url
                snippet
                attachments {
                    id
                    filename
                    email_id
                    url
                }
            }
        }
    `;
    const res = await axios.post(
        BASE_URI,
        {
            query,
        },
        {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        },
    );
    return res.data.data?.emailsWithAttachments;
};

export const getBills = async (accessToken: string, emailId: string) => {
    const query = `#graphql
        query MyQuery($emailId: String!) {
            billsByEmailId(emailId: $emailId) {
                id
                filename
            }
        }
    `;
    const res = await axios.post(
        BASE_URI,
        {
            query,
            variables: {
                emailId,
            },
        },
        {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        },
    );
    console.log(res.data);
    return res.data.data.billsByEmailId;
};
