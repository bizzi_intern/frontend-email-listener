import axios from 'axios';
import { BASE_URI } from '../lib/constants';

axios.defaults.withCredentials = true;

export const login = async (email: string, password: string) => {
    const query = `#graphql
        mutation loginUser($email: String!, $password: String!) {
            login(email: $email, password: $password) {
                accessToken
            }
        }
    `;
    const res = await axios.post(
        BASE_URI,
        {
            query,
            variables: {
                email,
                password,
            },
        },
        {
            headers: {
                'Content-Type': 'application/json',
            },
        },
    );
    return res.data.data.login;
};

export const refreshToken = async (email: string) => {
    const query = `#graphql
        mutation refreshToken($email: String!) {
            refreshToken(email: $email) {
                accessToken
            }
        }
    `;
    const res = await axios.post(
        BASE_URI,
        {
            query,
            variables: {
                email,
            },
        },
        {
            headers: {
                'Content-Type': 'application/json',
            },
        },
    );
    return res.data.data.refreshToken;
};

export const logout = async (email: string) => {
    const query = `#graphql
        mutation logoutUser($email: String!) {
            logout(email: $email) 
        }
    `;
    const res = await axios.post(
        BASE_URI,
        {
            query,
            variables: {
                email,
            },
        },
        {
            headers: {
                'Content-Type': 'application/json',
            },
        },
    );
    return res.data.data.logout;
};
