export const BASE_URI = String(process.env.REACT_APP_BASE_URI);
export const PORT = Number(process.env.PORT);
export const FIREBASE_API_KEY = String(process.env.FIREBASE_API_KEY);
export const FIREBASE_AUTH_DOMAIN = String(process.env.FIREBASE_AUTH_DOMAIN);
export const FIREBASE_PROJECT_ID = String(process.env.FIREBASE_PROJECT_ID);
export const FIREBASE_STORAGE_BUCKET = String(process.env.FIREBASE_STORAGE_BUCKET);
export const FIREBASE_MESSAGING_SENDER_ID = String(process.env.FIREBASE_MESSAGING_SENDER_ID);
export const FIREBASE_APP_ID = String(process.env.FIREBASE_APP_ID);
export const FIREBASE_MEASUREMENT_ID = String(process.env.FIREBASE_MEASUREMENT_ID);

export const FIREBASE_CONFIG = {
    apiKey: FIREBASE_API_KEY,
    authDomain: FIREBASE_AUTH_DOMAIN,
    projectId: FIREBASE_PROJECT_ID,
    storageBucket: FIREBASE_STORAGE_BUCKET,
    messagingSenderId: FIREBASE_MESSAGING_SENDER_ID,
    appId: FIREBASE_APP_ID,
    measurementId: FIREBASE_MEASUREMENT_ID,
};
