import classNames from 'classnames/bind';
import { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { getEmailsWithAttachments } from 'src/api/emailApi';
import Email from 'src/components/Email';
import { Part, Header, Email as EmailType } from 'src/types/types';
import styles from './Emails.module.scss';

const cx = classNames.bind(styles);

function Emails() {
    const [emails, setEmails] = useState([] as any);
    const [cookies, setCookie] = useCookies(["accessToken"]);
    useEffect(() => {
        getEmailsWithAttachments(cookies.accessToken).then(data => { 
            const sortedData = data?.sort((first: { date: Date }, second: { date: Date }) => {
                if (first.date > second.date) {
                    return -1;
                } else if (first.date < second.date) {
                    return 1;
                }
                return 0;
            })
            setEmails(sortedData) 
        })
    }, []);

    return (
        <div className={cx('wrapper')}>
            <h1 className={cx("title")}>Emails</h1>
            {emails?.length === 0 
                ? <div className={cx("loader")}></div>
                : emails?.map((email: EmailType) => 
                    <Email key={email.id} id={email.id} 
                        senderName={email.sender_name} from={email.from} 
                        date={email.date} subject={email.subject}
                        snippet={email.snippet} to={email.to}
                        contentUrl={email.content_url}
                        attachments={email.attachments} />
                )
            }
        </div>
    );
}

export default Emails;
