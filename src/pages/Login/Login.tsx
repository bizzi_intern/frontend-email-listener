import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom"
import classNames from 'classnames/bind';
import { useCookies } from "react-cookie"
import Form from 'src/components/Form_';
import styles from './Login.module.scss';
import { login } from 'src/api/authApi';
import BizziRobot from 'src/components/BizziRobot';
import BizziBox from 'src/components/BizziBox';

const cx = classNames.bind(styles);

function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [cookies, setCookie] = useCookies(["accessToken", "email", "isLoggedIn"]);
    const navigate = useNavigate();
    
    const validate = () => {
        if (email && password) {
            return true;
        }
        return false;
    }

    const handleLogin = async (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault()
        if (!validate()) {
            alert("Info not valid");
            return;
        } 
        const res = await login(email, password);
        if (res.accessToken) {
            setCookie("accessToken", res.accessToken);
            setCookie("email", email);
            setCookie("isLoggedIn", true);
            setIsLoggedIn(true)
        }
        else {
            alert("Log in failed")
        }
    }

    useEffect(() => {
        if (isLoggedIn) {
            navigate("/", {
                replace: true
            })
        }
    }, [isLoggedIn])

    return (
        <BizziBox>
            <div className={cx("login")}>
                <h1 className={cx("title")}>Log in</h1>
                <form className={cx("form")}>
                    <input 
                        value={email}
                        type="email"
                        name="email"
                        placeholder="Email"
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                    <input
                        value={password}
                        type="password" 
                        name="password"
                        placeholder="Password"
                        onChange={(e) => setPassword(e.target.value)}
                        required 
                    />
                    <button className={cx("submit-btn")} onClick={(e) => handleLogin(e)}>Log in</button>
                </form>
            </div>
        </BizziBox>
    );
}

export default Login;
