import classNames from 'classnames/bind';
import { useCookies } from 'react-cookie';
import { Link, useNavigate } from 'react-router-dom';
import { logout } from 'src/api/authApi';
import BizziBox from 'src/components/BizziBox';
import styles from './Home.module.scss';

const cx = classNames.bind(styles);

function Home() {
    const [cookies, setCookie] = useCookies(["accessToken", "email", "isLoggedIn"]);

    const handleLogout = async () => {
        const res = await logout(cookies.email);
        setCookie("accessToken", "");
        setCookie("email", "");
        setCookie("isLoggedIn", false);
    }

    return (
        <BizziBox>
            <div className={cx("home")}>
                <h1 className={cx("title")}>Home</h1>
                <div className={cx("btn-wrapper")}>
                    {cookies.isLoggedIn
                        ? 
                        <>
                            <Link to="/emails" className={cx("navigate-btn")}>Emails</Link>
                            <button className={cx("logout-btn")} onClick={handleLogout}>Log out</button>
                        </>
                        : <Link to="/login" className={cx("navigate-btn")}>Log in</Link>
                    }
                </div>
            </div>
        </BizziBox>
    );
}

export default Home;
