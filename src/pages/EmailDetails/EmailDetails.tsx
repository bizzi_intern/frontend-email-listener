import { ReactNode, useEffect, useState } from 'react';
import parser from "html-react-parser"
import classNames from 'classnames/bind';
import Attachment from 'src/components/Attachment';
import styles from "./EmailDetails.module.scss";
import { useLocation } from 'react-router-dom';
import { EmailProps } from 'src/types/types';
import { getEmailContent } from 'src/services/firebaseService';
import { getBills } from 'src/api/emailApi';
import { useCookies } from 'react-cookie';
import Bill from 'src/components/Bill';

const cx = classNames.bind(styles);

function EmailDetails() {
    const location = useLocation();
    const { id, to, from, senderName, date, subject, contentUrl, attachments }: Omit<EmailProps, "snippet"> = location.state;
    let init: any;
    const [content, setContent] = useState(init);
    const [bills, setBills] = useState()
    const [cookies] = useCookies(["accessToken"])

    useEffect(() => {
        getEmailContent(contentUrl).then((content) => setContent(parser(content)))
    }, [])
    // useEffect(() => {
    //     getBills(cookies.accessToken, id).then(billsList => setBills(billsList))
    // }, [attachments])

    return (
        <div className={cx('details-wrapper')}>
            <h1 className={cx("title")}>Email Details</h1>
            <h3 className={cx("subject")}>{subject}</h3>
            <h3 className={cx("sender")}>{senderName} {parser(`&lt${from}&gt`)}</h3>
            <p className={cx("date")}>{date.toString()}</p>
            <div className={cx("content")}>{content}</div>
            {attachments?.length > 0 
            ?   <div className={cx("attachments-wrapper")}>
                    <span className={cx("attachments-span")}>{attachments.length} Attachments</span>
                    <div className={cx("box")}>
                        {attachments.map(attachment => {
                            return <Attachment key={attachment.id}
                                        id={attachment.id}
                                        filename={attachment.filename}
                                        emailId={id}
                                        url={attachment.url} />
                        })}
                    </div>
                </div>
            : null
            }
            <div className={cx("bills-wrapper")}>
                <span className={cx("bills-span")}>3 Bills</span>
                <div className={cx("box")}>
                    <Bill />
                    <Bill />
                    <Bill />
                </div>
            </div>
        </div>
    );
}

export default EmailDetails;
