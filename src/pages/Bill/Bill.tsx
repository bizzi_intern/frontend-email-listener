import classNames from 'classnames/bind';
import styles from "./Bill.module.scss";

const cx = classNames.bind(styles);

function Bill() {
    
    return (
        <div className={cx('bill')}>
            <div>
                <h1 className={cx("title")}>Bill Details</h1>
                <div className={cx("id-wrapper")}>
                    <h3 className={cx("id")}>0077794</h3>
                </div>
            </div>
            <div className={cx("info")}>
                <div className={cx("info-column")}>
                    <span className={cx("info-title")}>Số hóa đơn</span>
                    <span className={cx("info-data")}>0077794</span>
                </div>
                <div className={cx("info-column")}>
                    <span className={cx("info-title")}>Ký hiệu</span>
                    <span className={cx("info-data")}>TM/20E</span>
                </div>
                <div className={cx("info-column")}>
                    <span className={cx("info-title")}>Mẫu số</span>
                    <span className={cx("info-data")}>01GTKT0/015</span>
                </div>
                <div className={cx("info-column")}>
                    <span className={cx("info-title")}>Ngày hóa đơn</span>
                    <span className={cx("info-data")}>30/11/2021</span>
                </div>
            </div>
            <div className={cx("seller")}>
                <p>Đơn vị bán hàng: <span>CHI NHÁNH CÔNG TY TNHH MỘT THÀNH VIÊN VIỄN THÔNG QUỐC TẾ FPT</span></p>
                <p>Mã số thuế: <span>0305793402-001</span></p>
                <p>Địa chỉ: <span>48 Vạn Bảo, P. Ngọc Khánh, Q. Ba Đình, TP. Hà Nội, Việt Nam</span></p>
                <p>Điện thoại: <span>024 883 4834</span></p>
            </div>
            <div className={cx("buyer")}>
                <p>Đơn vị mua hàng: <span>Cong ty Tai chinh Trach nhiem Huu han Mot thanh vien Home Credit Viet Nam</span></p>
                <p>Mã số thuế: <span>0307672788</span></p>
                <p>Địa chỉ:&nbsp;
                    <span>
                        Tang G, 8 va 10 Toa Nha Phu Nu, so 20 Nguyen Dang Giai, Phuong Thao Dien, Thanh Pho Thu Duc, TP. Ho Chi Minh
                    </span>
                </p>
                <p>Hình thức thanh toán: <span>Tiền mặt/Chuyển khoản</span></p>
            </div>
            <table className={cx("services")}>
                <tr>
                    <th>STT</th>
                    <th>Tên hàng hóa, dịch vụ</th>
                    <th>Đơn vị tính</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th>Thành tiền chưa có thuế GTGT</th>
                </tr>
                <tr className={cx("sub-header-row")}>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6=4x5</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Cuoc thue kenh</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td className={cx("align-right")}>3.900.000</td>
                </tr>
                <tr>
                    <td colSpan={5} className={cx("align-right")}>Tổng tiền chưa có thuế GTGT</td>
                    <td className={cx("align-right")}>3.900.000</td>
                </tr>
                <tr>
                    <td colSpan={5} className={cx("align-right")}>Tổng tiền thuế GTGT 10%</td>
                    <td className={cx("align-right")}>390.000</td>
                </tr>
                <tr>
                    <td colSpan={5} className={cx("align-right")}>Tổng tiền thanh toán đã có thuế GTGT</td>
                    <td className={cx("total", "align-right")}>4.290.000</td>
                </tr>
                <tr>
                    <td colSpan={6} className={cx("align-left")}>Số tiền viết bằng chữ: <span className={cx("total-in-words")}>[+] Bon trieu hai tram chin muoi nghin dong chan.</span></td>
                </tr>
            </table>
            <div className={cx("signing")}>
                <div className={cx("seller-signing")}>Người mua hàng</div>
                <div className={cx("buyer-signing")}>Người bán hàng</div>
            </div>
        </div>
    );
}

export default Bill;
