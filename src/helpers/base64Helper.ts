export function decodeBase64ToString(base64: string) {
    const text = atob(base64.replace(/\-/g, '+').replace(/\_/g, '/'));
    const length = text.length;
    const bytes = new Uint8Array(length);
    for (let i = 0; i < length; i++) {
        bytes[i] = text.charCodeAt(i);
    }
    const decoder = new TextDecoder(); // default is utf-8
    return decoder.decode(bytes);
}

export function decodeBase64ToBytes(base64: string) {
    const text = atob(base64.replace(/\-/g, '+').replace(/\_/g, '/'));
    const length = text.length;
    const bytes = new Uint8Array(length);
    for (let i = 0; i < length; i++) {
        bytes[i] = text.charCodeAt(i);
    }
    return bytes;
}

export function decodeBytesToString(bytes: Uint8Array) {
    const decoder = new TextDecoder(); // default is utf-8
    return decoder.decode(bytes);
}
