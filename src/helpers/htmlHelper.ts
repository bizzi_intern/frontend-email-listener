export function decodeHtml(html: string) {
    let txt = document.createElement('textarea');
    txt.innerHTML = html;
    return txt.value;
}

export function decodeContent(html: string) {
    const doc = new DOMParser().parseFromString(html, 'text/html');

    return doc;
}
