import classNames from 'classnames/bind';
import styles from './BizziBox.module.scss';
import { Props } from 'src/types/types';
import BizziRobot from '../BizziRobot';

const cx = classNames.bind(styles);

function BizziBox({ children }: Props) {
    return (
        <div className={cx("wrapper")}>
            <div className={cx("bizzi-box")}>
                <BizziRobot className={cx("robot")} />
                {children}
            </div>
        </div>
    );
}

export default BizziBox;