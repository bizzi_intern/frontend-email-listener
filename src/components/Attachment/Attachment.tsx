import classNames from 'classnames/bind';
import { useEffect } from 'react';
import { useNavigate } from "react-router-dom"
import { Link } from 'react-router-dom';
import { getExtension } from 'src/helpers/fileHelper';
import styles from './Attachment.module.scss';

const cx = classNames.bind(styles);

function Attachment({ id, filename, emailId, url }: { id: string, filename: string, emailId: string, url: string }) {
    const extension = getExtension(filename);
    const navigate = useNavigate();
    let img = "";
    if (extension === "pdf") {
        img = "./pdf_icon.png"
    } else if (extension === "xlsx" || extension === "xls") {
        img = "./excel_icon.png"
    } else if (extension === "xml" || extension === "docx" || extension === "doc") {
        img = "./text_icon.png"
    }
    const state = {
        id,
        filename,
        emailId,
        url
    }

    const handleClick = () => {
        navigate("/emails/", {
            replace: true,
            state
        })
    }

    return (            
        <button className={cx("attachment")} onClick={handleClick}>
            <img className={cx("icon")} src={img} />
            <span className={cx("filename")}>{filename}</span>
        </button>
    );
}

export default Attachment;