import classNames from 'classnames/bind';
import { EmailProps, Part } from 'src/types/types';
import styles from './Email.module.scss';
import { decodeHtml } from 'src/helpers/htmlHelper';
import Attachment from '../Attachment';
import { Link } from 'react-router-dom';

const cx = classNames.bind(styles);

function Email({ id, to, from, senderName, date, subject, contentUrl, snippet,  attachments = [] }: EmailProps) {
    const state = {
        id, to, from, senderName, date, subject, contentUrl, snippet,  attachments
    }

    return (            
        <Link to={id} className={cx("email-wrapper")} state={state}>
            <div className={cx("sender-date-container")}>
                <h3 className={cx("sender")}>{senderName}</h3>
                <p className={cx("date")}>{date.toString()}</p>
            </div>
            <div className={cx("content")}>
                <h3 className={cx("subject")}>{subject}</h3>
                <p className={cx("snippet")}>{decodeHtml(snippet)}</p>
                <div className={cx("attachments-wrapper")}>
                    {attachments?.length > 0 
                        ? attachments.map(attachment => {
                            return <Attachment key={attachment.id}
                                        id={attachment.id}
                                        filename={attachment.filename}
                                        emailId={attachment.email_id}
                                        url={attachment.url} />
                        })
                        : null
                    }
                </div>
            </div>
        </Link>
    );
}

export default Email;