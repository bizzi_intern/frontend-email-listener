import classNames from 'classnames/bind';
import styles from './Bill.module.scss';
import { useNavigate } from 'react-router-dom';

const cx = classNames.bind(styles);

function Bill() {
    const navigate = useNavigate();
    const handleClick = () => {
        navigate("/bills", {
            replace: true,
        })
    }

    return (          
        <div className={cx("bill")} onClick={handleClick}>
            <img src="./bill.png" alt="bill" />
            <p className={cx("bill-id")}>0239238</p>
        </div>
    );
}

export default Bill;