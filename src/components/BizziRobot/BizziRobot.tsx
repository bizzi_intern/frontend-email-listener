import classNames from 'classnames/bind';
import styles from './BizziRobot.module.scss';

const cx = classNames.bind(styles);

function BizziRobot({ className }: { className?: string }) {
    return ( 
        <div className={cx("robot-wrapper") + " " + className}>
            <img className={cx("background")} src="./bizzi-robot-background.svg" alt="Bizzi Robot Background" />
            <img className={cx("robot")} src="./bizzi-robot.svg" alt="Bizzi Robot" />
        </div>
    );
}

export default BizziRobot;