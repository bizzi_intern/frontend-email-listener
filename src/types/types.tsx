export type Props = {
    children?: any
    title?: string
}

export type Header = {
    name: string
    value: string
}

export type Body = {
    attachmentId?: string
    size: number
    data?: string
}

export type Part = {
    partId: string
    mimeType: string
    filename?: string
    headers: Array<Header>
    body: Body
    parts?: Array<Part>
}

export type EmailProps = {
    id: string
    to: string
    from: string
    senderName: string
    date: Date
    subject: string
    contentUrl: string;
    snippet: string
    attachments: Attachment[]
}

export type RouteProperties = {
    path: string
    page: React.FC<any>
    isAuthenticated?: boolean
}

export type Attachment = {
    id: string;
    filename: string;
    url: string;
    email_id: string;
  };
  

export type Email = {
    id: string
    to: string
    from: string
    sender_name: string
    date: Date
    subject: string
    content_url: string
    snippet: string
    attachments: Attachment[]
}