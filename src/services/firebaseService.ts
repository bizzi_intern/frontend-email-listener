// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getBytes, getStorage, ref } from 'firebase/storage';
import { decodeBytesToString } from 'src/helpers/base64Helper';
import { FIREBASE_CONFIG } from '../lib/constants';
import { decodeContent, decodeHtml } from '../helpers/htmlHelper';

// Initialize Firebase
const firebase = initializeApp(FIREBASE_CONFIG);

// Initialize Cloud Storage and get a reference to the service
const storage = getStorage(firebase);

async function getEmailContent(url: string) {
    const emailRef = ref(storage, url);
    const buffer = await getBytes(emailRef);
    const bytes = new Uint8Array(buffer);
    // const content = decodeContent(decodeBytesToString(bytes));
    // console.log(content);
    return decodeBytesToString(bytes);
}

export { firebase, storage, getEmailContent };
