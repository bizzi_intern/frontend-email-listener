import { RouteProperties } from 'src/types/types';

// Pages
import Home from 'src/pages/Home';
import Login from 'src/pages/Login';
import Email from 'src/pages/Emails';
import EmailDetails from 'src/pages/EmailDetails';
import Bill from 'src/pages/Bill';

const publicRoutes: Array<RouteProperties> = [
    { path: "/", page: Home },
    { path: "/login", page: Login },
    { path: "/emails", page: Email },
    { path: "/bills", page: Bill },
    { path: "/emails/:id", page: EmailDetails },
    { path: "/emails/:email_id/attachments/:attachment_id", page: Bill }
];

const privateRoutes: Array<RouteProperties> = [
    // { path: config.routes.user, page: User },
    // { path: config.routes.profile, page: Profile },
];

export { privateRoutes, publicRoutes};
